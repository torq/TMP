#include <iostream>
#include <cmath>

// based on https://www.boost.org/doc/libs/1_69_0/libs/geometry/doc/html/geometry/design.html

struct mypoint {
    double x, y;
};

// element access
namespace traits {
	template <typename P, int D>
	struct access {};

	// specialization for mypoint x element access
	template <>
	struct access<mypoint, 0> {
		static double get(const mypoint& a) {
			return a.x;
		}
	};

	// specialization for mypoint y element access
	template <>
	struct access<mypoint, 1> {
		static double get(const mypoint& a) {
			return a.y;
		}
	};
}

// Flex the number of dimensions of a point
namespace traits {
    template <typename P>
    struct dimension {};

    template <>
    struct dimension<mypoint> {
    	static constexpr int value = 2;
    };
}

// Flex the coordinate type
namespace traits {
	template <typename P>
	struct coordinate_type {};

	// specialization for mypoint coordinate type
	template<>
	struct coordinate_type<mypoint> {
		typedef double type;
	};
}

// Get the most precise coordinate type
namespace traits {
	template <typename P1, typename P2>
	struct select_most_precise {};

	// TODO: Is there no way of making a template that if P1 == P2, then typedef P1 type?

	// specialization for double
	template <>
	struct select_most_precise<double, double> {
		typedef double type;
	};

	// specialization for int
	template <>
	struct select_most_precise<int, int> {
		typedef int type;
	};

	// specialization for double and int
	template <>
	struct select_most_precise<double, int> {
		typedef double type;
	};

	template <>
	struct select_most_precise<int, double> {
		typedef double type;
	};
}

// Trait for tag dispatching
namespace traits {
	struct point_tag {};

	template <typename G>
	struct tag {};

	// specialization for mypoint
	template <>
	struct tag<mypoint> {
		typedef point_tag type;
	};
}

// algorithms for distance calculation by tag dispatching
namespace dispatch {
	namespace algorithms {
		// helper for select_most_precise
		template <typename P1, typename P2>
		struct select_most_precise : traits::select_most_precise<P1, P2> {};

		// helper for coordinate_type
		template <typename P>
		struct coordinate_type : traits::coordinate_type<P> {};

		// helper for get
		template <int D, typename P>
		double get(const P& p) {
			return traits::access<P,D>::get(p);
		}

		template <typename P1, typename P2, int D>
		struct pythagoras {
			typedef typename select_most_precise<
				typename coordinate_type<P1>::type,
				typename coordinate_type<P2>::type
			>::type computation_type;

			static computation_type apply(const P1& a, const P2& b) {
				computation_type d = get<D-1>(a) - get<D-1>(b);
				return d*d + pythagoras<P1,P2,D-1>::apply(a, b);
			}
		};

		template <typename P1, typename P2 >
		struct pythagoras<P1, P2, 0> {
			typedef typename select_most_precise<
				typename coordinate_type<P1>::type,
				typename coordinate_type<P2>::type
			>::type computation_type;

		    static computation_type apply(P1 const&, P2 const&) {
		        return 0;
		    }
		};
	}

	// helper for dimension
	template <typename P>
	struct dimension : traits::dimension<P> {};

	//helpers for tags
	template <typename G>
	struct tag : traits::tag<G> {};

	// generic distance class template
	template <typename Tag1, typename Tag2, typename G1, typename G2>
	struct distance {};

	// specialization for distance between points
	template <typename P1, typename P2>
	struct distance <traits::point_tag, traits::point_tag, P1, P2> {
		static double apply(const P1& a, const P2& b) {
			static_assert(dimension<P1>::value == dimension<P2>::value, "Both dimensions should be the same.");

		    return std::sqrt(algorithms::pythagoras<P1, P2, dimension<P1>::value>::apply(a, b));
		}
	};
}

template <typename G1, typename G2>
double distance(const G1& a, const G2& b) {
	return dispatch::distance<
        typename traits::tag<G1>::type,
        typename traits::tag<G2>::type,
        G1, G2
    >::apply(a, b);
}

int main() {
    mypoint a { 0.0, 0.0 };
    mypoint b { 1.0, 1.0 };

    auto d = distance(a, b);
    std::cout << d << std::endl;

    return 0;
}
